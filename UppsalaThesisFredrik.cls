\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{UppsalaThesis}[2015/03/28 v1.1 Uppsala University Licentiate Thesis Theme]

\newif\if@paperAcode\@paperAcodefalse
\newif\if@paperScode\@paperScodetrue
\DeclareOption{draft}{%
	\@paperAcodetrue
	\@paperScodefalse
}
\DeclareOption{a4paper}{%
	\@paperAcodetrue
	\@paperScodefalse
}

\ProcessOptions\relax
%\LoadClass{book}
% Causes the options of this class to be passed to the book class
% Needed to change the text font and other behaviours.
\LoadClassWithOptions{book} 

\DeclareFontShape{OT1}{cmr}{bx}{sc}{<-> cmbcsc10}{}

% -------------------------------------------------------------------------------------
\RequirePackage{graphicx} %needed for covers
\RequirePackage{color}
% -------------------------------------------------------------------------------------
% C H A P T E R   H E A D I N G AND P A G E   S T Y L E
% 
\renewcommand\frontmatter{
	\pagenumbering{roman}
	\pagestyle{plain}
}
\renewcommand\mainmatter{
	\pagenumbering{arabic}
	\pagestyle{fancy}
	% The lines below are needed so that the empty pages after the frontmater are completely empty.
	\let\origdoublepage\cleardoublepage
	\newcommand{\clearemptydoublepage}{\clearpage{\pagestyle{empty}\origdoublepage}}
	\let\cleardoublepage\clearemptydoublepage
}

\RequirePackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}
\fancyhead[LO]{\nouppercase{\rightmark}}
\fancyhead[RE]{\nouppercase{\leftmark}}
\fancyhead[LE,RO]{\thepage}
\fancyfoot{}

%%%%%%%%% NEW ENVS
%%%%%%% ABSTRACT
\newenvironment{abstract}
{
	\thispagestyle{plain}
	\begin{center}
		\vspace*{1.5cm}
		{\Large\bfseries Abstract}
	\end{center}
	\vspace{0.5cm}
	\begin{quote}
}
{%
	\end{quote}
	\cleardoublepage
}
%%%%%%% ACKNOWLEDGMENT
\newenvironment{acknowledgments}
{
	\thispagestyle{plain}
	\vspace*{1.5cm}
	{\huge\bfseries Acknowledgments}	
	\vspace{0.5cm}
	\begin{quote}
}
{%
	\end{quote}
	\cleardoublepage
}

%%%%%%% LIST OF PAPERS
\newenvironment{listofpapers}
{
	\thispagestyle{plain}
	\vspace*{1.5cm}
	{\huge\bfseries List of Papers}
	\vspace{0.5cm}
	\begin{quote}
}
{%
	\end{quote}
	\cleardoublepage
}


%%%%%%%%%%%%%%%%%
%%%%% Titlepage
\let\@spinetitle\@empty
\let\@email\@empty
\let\@division\@empty
\let\@department\@empty
\let\@university\@empty
\let\@address\@empty
\let\@issn\@empty
\let\@printer\@empty
\let\@degree\@empty
\let\@web\@empty
\let\@logo\@empty
\let\@logosv\@empty
\let\@logosvback\@empty
\let\@logogr\@empty

\newcommand{\spinetitle}[1]{\def\@spinetitlename{#1}}\spinetitle{\@title}
\newcommand{\thesisnumber}[1]{\def\@thesisnum{#1}}\thesisnumber{2015-xxx}
\newcommand{\thesissubtitle}[1]{\def\@thesissubtitlename{#1}}\thesissubtitle{}
\newcommand{\logo}[1]{\def\@logoname{#1}}\logo{./logo_name.pdf}
\newcommand{\logosv}[1]{\def\@logonamesv{#1}}\logosv{./logo_name_sv.pdf}
\newcommand{\logogr}[1]{\def\@logonamegr{#1}}\logogr{./logo_grey.pdf}
\newcommand{\email}[1]{\def\@emailname{#1}}\email{My email}
\newcommand{\division}[1]{\def\@divisionname{#1}}\division{Division of Scientific Computing}
\newcommand{\department}[1]{\def\@departmentname{#1}}\department{Department of Information Technology}
\newcommand{\university}[1]{\def\@universityname{#1}}\university{Uppsala University}
\newcommand{\address}[1]{\def\@addressname{#1}}\address{Box 337\\ SE-751 05 Uppsala\\ Sweden}
\newcommand{\issn}[1]{\def\@issnname{#1}}\issn{ISSN 1404-5117}
\newcommand{\degree}[1]{\def\@degreename{#1}}\degree{Dissertation for the degree of Licentiate of Technology in Scientific Computing with specialization in Numerical Analysis}
\newcommand{\printer}[1]{\def\@printername{#1}}\printer{Department of Information Technology, Uppsala University, Sweden}
\newcommand{\web}[1]{\def\@webname{#1}}\web{http://www.it.uu.se/}

\if@paperAcode 
	\def\@logo{\includegraphics[width=30mm]{\@logoname}}
	\def\@logogr{\includegraphics[width=170mm]{\@logonamegr}} 
	\def\@logosv{\includegraphics[width=40mm]{\@logonamesv}}
	\def\@logosvback{\includegraphics[width=30mm]{\@logonamesv}}
\fi
\if@paperScode 
	\def\@logo{\includegraphics[width=25mm]{\@logoname}}
	\def\@logogr{\includegraphics[width=135mm]{\@logonamegr}} 
	\def\@logosv{\includegraphics[width=35mm]{\@logonamesv}}
	\def\@logosvback{\includegraphics[width=25mm]{\@logonamesv}}
\fi
\def\@spinetitle{\@spinetitlename}
\def\@thesisnumber{\@thesisnum}
\def\@thesissubtitle{\@thesissubtitlename}
\def\@email{\@emailname}
\def\@division{\@divisionname}
\def\@department{\@departmentname}
\def\@university{\@universityname}
\def\@address{\@addressname}
\def\@issn{\@issnname}
\def\@printer{\@printername}
\def\@web{\@webname}
\def\@degree{\@degreename}

\def\@timesfont #1#2{\fontfamily{ptm}\fontsize{#1}{#2pt}\selectfont}

%%%%%%%%% S5 PAPER
\if@paperScode % if true, execute the following instructions until \fi
  	\RequirePackage{s5paper}
\fi

% Paper resizing commands
\providecommand{\UU@setsfivepaper}{
	\setlength{\paperheight}{242mm} 
	\setlength{\paperwidth}{165mm}
	\setlength{\hoffset}{-1in}
	\setlength{\voffset}{-1in}
	\setlength{\topmargin}{8.8mm} 
	\setlength{\oddsidemargin}{22.5mm}
	\setlength{\evensidemargin}{22.5mm}
	\setlength{\headheight}{4.6mm}
	\setlength{\headsep}{4.6mm}
	\setlength{\textheight}{572pt}
	\setlength{\textwidth}{120mm}
	\setlength{\marginparsep}{1mm}
	\setlength{\marginparwidth}{20mm}
	\setlength{\footskip}{26pt}
	\setlength{\topskip}{13pt}
	\setlength{\pdfpagewidth}{\paperwidth}
	\setlength{\pdfpageheight}{\paperheight}
}
\providecommand{\UU@setathreepaper}{
	\setlength{\paperheight}{297mm} 
	\setlength{\paperwidth}{420mm}
	\setlength{\hoffset}{3mm}
	\setlength{\voffset}{5mm}
	\setlength{\topmargin}{0mm} 
	\setlength{\oddsidemargin}{0mm}
	\setlength{\evensidemargin}{0mm}
	\setlength{\headheight}{0mm}
	\setlength{\headsep}{0mm}
	\setlength{\textheight}{242mm}
	\setlength{\textwidth}{340mm}
	\setlength{\marginparsep}{0mm}
	\setlength{\marginparwidth}{0mm}
	\setlength{\footskip}{0mm}
	\setlength{\topskip}{0mm}
	\setlength{\pdfpagewidth}{\paperwidth}
	\setlength{\pdfpageheight}{\paperheight}
}

%%%\if@paperScode % if true, execute the following instructions until \fi
%%%\AtBeginDocument{
%%%  \UU@setsfivepaper
%%%  \special{papersize=\the\pdfpagewidth,\the\pdfpageheight}
%%%}
%%%\fi

%%%%% END S5 PAPER

\if@paperAcode % if true, execute the following instructions until \fi
  	\RequirePackage{a4wide}
\fi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% C O V E R   P A G E
\def\makecover{\begingroup
	% set paper size
	\UU@setathreepaper
	\special{papersize=\the\pdfpagewidth,\the\pdfpageheight}
	%% Remove page number
	\setlength{\unitlength}{1mm}
	\parskip 0mm
	\parindent 0mm
	\thispagestyle{empty} %left
	\pagenumbering{gobble}
	\begin{picture}(0,0)(-10,242) %% Back cover
		% \put(0,242){\rule{165mm}{0.2mm}}
		\put(19,218){\parbox[t]{12.7cm}{\raggedright
		  \@timesfont{9}{11}\textbf{Recent licentiate theses from the~\@department}\linebreak
		  \vskip 3mm\begin{tabular}{@{}lp{103mm}}\@thesisbacklist\end{tabular}}}
		\protect\put(76,40){\makebox(0,0){\@logosvback}}
		\protect\put(76,18.5){\makebox(0,0){\parbox[b]{16cm}{\centering\@timesfont{10}{14}\@printer}}}
		% \rule{165mm}{0.2mm}
		% \rule{0.2mm}{242mm}
	\end{picture}
	\begin{picture}(0,0)(-175,242) % Spine
		% \put(0,242){\rule{10mm}{0.2mm}}
    	\put(2,220){
    	\rotatebox{270}{
       		\@timesfont{10}{12}\MakeUppercase{
       		\@author}
       		\hspace{10mm}
       		{\@timesfont{10}{12}\@spinetitle}
       	}
       	}
		% \rule{10mm}{0.2mm}
		% \rule{0.2mm}{242mm}
	\end{picture}
	\begin{picture}(0,0)(-185,242) %Front Cover
		% \put(0,242){\rule{165mm}{0.2mm}}
		\put(18,202){\@logosv}
		\put(5,197){\makebox(0,0)[tl]{\line(1,0){155}}}
		\put(90,-25){\@logogr}
		\put(18,184){\parbox[tl]{12.7cm}{\raggedright\@timesfont{12}{14}IT Licentiate theses\linebreak\@thesisnumber}}
		\put(18,130){\parbox[bl]{12.7cm}{\@timesfont{20}{24}\raggedright \@title\vskip 2mm\@timesfont{16}{18}{\@thesissubtitle}}}
		\put(18,100){\parbox[tl]{12.8cm}{\@timesfont{20}{24}\textsc{\@author}}}
		\put(18,20){\parbox[bl]{12.8cm}{\raggedright\@timesfont{12}{14}\MakeUppercase{\@university}\linebreak
		\@department}}
		% \rule{165mm}{0.2mm}
		% \rule{0.2mm}{242mm}
	\end{picture}
	\null
	\clearpage	
\endgroup}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BACKPAGE
\def\thesisbackelm #1#2#3{\textbf{#1}&#2: \textit{#3}\\ [2mm]}
\def\@thesisbacklist{\thesisbackelm{1999-00X}{Author's Name}{An Extremely Interesting Licentiate Thesis}}
\def\thesisbacklist #1{\gdef\@thesisbacklist{#1}}

\def\makelicback{
	\pagenumbering{gobble}
	\cleardoublepage % Finish the inside text
	\clearpage\newpage\hbox{}\thispagestyle{empty}\newpage		 % Go to the outside of the cover
	%%
	\oddsidemargin 0mm
	\evensidemargin 0mm
	\parskip 0mm
	\parindent 0mm
	\setlength{\unitlength}{1mm}
	\thispagestyle{empty} %left
	\if@paperAcode 
	\null\begin{picture}(0,0)(25,240) %put origo at PostScript 0 0 (left page)
	\put(35,255){\parbox[t]{16cm}{\raggedright
	  \@timesfont{12}{16}\textbf{Recent licentiate theses from the~\@department}\linebreak
	  \vskip -3mm\begin{tabular}{@{}lp{132mm}}\@thesisbacklist\end{tabular}}}
	\protect\put(100,48){\makebox(0,0){\@logosvback}}
	\protect\put(100,20){\makebox(0,0){\parbox[b]{16cm}{\centering\@timesfont{12}{16}\@printer}}}
	\end{picture}\vfil\null
	\fi
	\if@paperScode 
	\null\begin{picture}(0,0)(15,165) %put origo at PostScript 0 0 (left page)
	\put(40,165){\parbox[t]{12.7cm}{\raggedright
	  \@timesfont{10}{12}\textbf{Recent licentiate theses from the~\@department}\linebreak
	  \vskip -3mm\begin{tabular}{@{}lp{100mm}}\@thesisbacklist\end{tabular}}}
	\protect\put(100,0){\makebox(0,0){\@logosvback}}
	\protect\put(100,-22){\makebox(0,0){\parbox[b]{16cm}{\centering\@timesfont{10}{14}\@printer}}}
	\end{picture}\vfil\null
	\fi
}

%%%%%%%%%% F R O N T   P A G E %%%%%%%%%%%%%

\def\@firstTitle{
	%\newgeometry{top=1cm,bottom=0.1cm}
	\setlength{\unitlength}{1mm}
	\thispagestyle{empty}
	\pagenumbering{gobble}
	\if@paperAcode 
		\begin{picture}(0,0)(20,240)
			\put(10,230){\@logosv}
			\put(-10,220){\makebox(0,0)[tl]{\line(1,0){198}}}
			\put(95,-34){\@logogr}
			\put(10,203){\parbox[tl]{16cm}{\raggedright\@timesfont{14}{16}IT Licentiate theses\linebreak\@thesisnumber}}
			\put(10,142){\parbox[bl]{16cm}{\@timesfont{25}{30}\raggedright \@title\vskip 2mm\@timesfont{18}{22}{\@thesissubtitle}}}
			\put(10,110){\parbox[tl]{16cm}{\@timesfont{18}{22}\textsc{\@author}}}
			\put(10,15){\parbox[bl]{16cm}{\raggedright\@timesfont{14}{16}\MakeUppercase{\@university}\linebreak
			  \@department}}
		\end{picture}
		\null\vfill
	\fi
	\if@paperScode 
		\begin{picture}(0,0)(15,165)
			\put(10,143){\@logosv}
			\put(-5,138){\makebox(0,0)[tl]{\line(1,0){150}}}
			\put(73,-68){\@logogr}
			\put(10,123){\parbox[tl]{12.7cm}{\raggedright\@timesfont{12}{14}IT Licentiate theses\linebreak\@thesisnumber}}
			\put(10,68){\parbox[bl]{12.7cm}{\@timesfont{20}{24}\raggedright \@title\vskip 2mm\@timesfont{16}{18}{\@thesissubtitle}}}
			\put(10,43){\parbox[tl]{12.8cm}{\@timesfont{16}{18}\textsc{\@author}}}
			\put(10,-25){\parbox[bl]{12.8cm}{\raggedright\@timesfont{12}{14}\MakeUppercase{\@university}\linebreak
			\@department}}
		\end{picture}
		\null\vfill
	\fi
	\cleardoublepage
}

\def\@secondTitle{
	\setlength{\unitlength}{1mm}
	\thispagestyle{empty}
	\pagenumbering{gobble}
	\begin{center}
	\if@paperAcode 
		\vspace*{14mm} 
		{\par\@logo\par}
		\vspace*{30mm}
		{\fontfamily{ptm}\fontsize{14.4}{10pt}\selectfont\@title\par}
		\vspace*{14mm}
		{\itshape\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@author\par}
		{\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@email\par}
		\vspace*{6mm}
		{\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@date\par}
		\vspace*{14mm}
		{\ifx\@division\@empty\else\itshape\par\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@division\fi}
		{\ifx\@department\@empty\else\itshape\par\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@department\linebreak\fi}
		{\ifx\@university\@empty\else\itshape\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@university\linebreak\fi}
		{\ifx\@address\@empty\else\itshape\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@address\par\fi}
		\vspace*{6mm}
		{\ifx\@web\@empty\else\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@web\par\fi}
		\vspace*{30mm}
		{\ifx\@degree\@empty\else\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@degree\par\fi}
		\vspace*{10mm}
		{\ifx\@author\@empty\else\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\textcopyright~\@author~\the\year\par\fi}
		{\ifx\@issn\@empty\else\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@issn\par\fi}
		{\ifx\@printer\@empty\else\fontfamily{ptm}\fontsize{12}{10pt}\selectfont Printed by the~\@printer\par\fi}
	\fi
	\if@paperScode 
		\vspace*{10mm} 
		{\par\@logo\par}
		\vspace*{24mm}
		{\fontfamily{ptm}\fontsize{12}{10pt}\selectfont\@title\par}
		\vspace*{12mm}
		{\itshape\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@author\par}
		{\fontfamily{ptm}\fontsize{9}{10pt}\selectfont\@email\par}
		\vspace*{5mm}
		{\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@date\par}
		\vspace*{14mm}
		{\ifx\@division\@empty\else\itshape\par\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@division\fi}
		{\ifx\@department\@empty\else\itshape\par\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@department\linebreak\fi}
		{\ifx\@university\@empty\else\itshape\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@university\linebreak\fi}
		{\ifx\@address\@empty\else\itshape\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@address\par\fi}
		\vspace*{5mm}
		{\ifx\@web\@empty\else\fontfamily{ptm}\fontsize{9}{10pt}\selectfont\@web\par\fi}
		\vspace*{24mm}
		{\ifx\@degree\@empty\else\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@degree\par\fi}
		\vspace*{8mm}
		{\ifx\@author\@empty\else\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\textcopyright~\@author~\the\year\par\fi}
		{\ifx\@issn\@empty\else\fontfamily{ptm}\fontsize{10}{10pt}\selectfont\@issn\par\fi}
		{\ifx\@printer\@empty\else\fontfamily{ptm}\fontsize{10}{10pt}\selectfont Printed by the~\@printer\par\fi}
	\fi
	\end{center}
	\null\vfill
	\cleardoublepage
}

\AtBeginDocument{% This is needed so that \maketitle works
\renewcommand\maketitle{
	\@firstTitle
	\@secondTitle
}
}

%%%%%%%%%%%%%%% A D D   P A P E R S  , C O U N T E R S %%%%%%%%%%%%%%
\newcounter{paperCounter}
\newcounter{rulePlacement}
\newcounter{textPlacement}
\newcounter{rangeCounter}
\if@paperScode
\addtocounter{rulePlacement}{144}
\addtocounter{textPlacement}{148}
\fi
\if@paperAcode
\addtocounter{rulePlacement}{230}
\addtocounter{textPlacement}{234}
\fi

\def\addpaperrange #1{
	\setlength{\unitlength}{1mm}
	\setcounter{rangeCounter}{0}
	\addtocounter{rangeCounter}{\value{paperCounter}}
	\addtocounter{rangeCounter}{#1}
	\loop\ifnum\value{paperCounter}<\value{rangeCounter}
	\cleardoublepage
	\thispagestyle{empty}
	\pagenumbering{gobble}
	\stepcounter{paperCounter}
	\addtocounter{rulePlacement}{-18} %first one is 130
	\addtocounter{textPlacement}{-18} %first one is 134
	\if@paperScode
	\begin{picture}(0,0)(15,165)
	\put(87,\therulePlacement){\rule{6.5cm}{1.2cm}}
	\put(93,\thetextPlacement){\color{white}\@timesfont{20}{22}\selectfont\fontdimen2\font=0.1ex P a p e r\hspace{0.5em}\Roman{paperCounter}\color{black}}
	\end{picture}
	\fi
	\if@paperAcode
	\begin{picture}(0,0)(20,240)
	\put(130.5,\therulePlacement){\rule{6.5cm}{1.2cm}}
	\put(136.5,\thetextPlacement){\color{white}\@timesfont{20}{22}\selectfont\fontdimen2\font=0.1ex P a p e r\hspace{0.5em}\Roman{paperCounter}\color{black}}
	\end{picture}
	\fi
	\null\vfill
	\cleardoublepage
	\repeat
}

\def\addpaper {
	\setlength{\unitlength}{1mm}
	\cleardoublepage
	\thispagestyle{empty}
	\pagenumbering{gobble}
	\stepcounter{paperCounter}
	\addtocounter{rulePlacement}{-18} %first one is 130
	\addtocounter{textPlacement}{-18} %first one is 134
	\if@paperScode
	\begin{picture}(0,0)(15,165)
	\put(87,\therulePlacement){\rule{6.5cm}{1.2cm}}
	\put(93,\thetextPlacement){\color{white}\@timesfont{20}{22}\selectfont\fontdimen2\font=0.1ex P a p e r\hspace{0.5em}\Roman{paperCounter}\color{black}}
	\end{picture}
	\fi
	\if@paperAcode
	\begin{picture}(0,0)(20,240)
	\put(130.5,\therulePlacement){\rule{6.5cm}{1.2cm}}
	\put(136.5,\thetextPlacement){\color{white}\@timesfont{20}{22}\selectfont\fontdimen2\font=0.1ex P a p e r\hspace{0.5em}\Roman{paperCounter}\color{black}}
	\end{picture}
	\fi
	\null\vfill
	\cleardoublepage
}



% -------------------------------------------------------------------------------------
% % (RE)NEW COMMANDS
% % 
% % Rename bibliography to references.
% \renewcommand{\bibname}{References}

% % Define a new command to include the bibliography file and 
% % set the formatting options.
% \newcommand{\includeBib}[1]
% {%
% 	\clearpage                                 % Fix the page number in TOC.
% 	\phantomsection                            % Fix the link in PDF.
% 	\addcontentsline{toc}{chapter}{References} % Add the bibliography to TOC.
% 	\bibliographystyle{abbrv}                  % Set the bibliography style.
% 	\bibliography{#1}                          % Include the bibliography file.
% }

% -------------------------------------------------------------------------------------
% F O N T S E L E C T I O N
%%% Check if the document is built with xelatex and load the Times new roman font
\RequirePackage{ifxetex}
\ifxetex
   \RequirePackage{fontspec}
   \setmainfont{Times New Roman}
\fi

\endinput
