% This text is proprietary.

\documentclass[fyma,slideColor,colorBG,final,pdf,10pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm,algorithmicx,algpseudocode}
\usepackage{verbatim}
\usepackage{graphicx,color}
\usepackage{graphics}
\usepackage{subfigure}
\usepackage{epstopdf} 
\usepackage{mathrsfs}
\usepackage{multicol}
\usepackage{animate}
%\usepackage[retainorgcmds]{IEEEtrantools}

\usecolortheme[RGB={153, 0, 0}]{structure}
\xdefinecolor{uppsalared}{RGB}{153,0,0}
\setbeamercolor{headline}{bg=uppsalared,fg=white}
\usetheme{umbc1}
\xdefinecolor{uppsalagreen}{RGB}{0,80,0}
\xdefinecolor{darkgreen}{rgb}{0, 0.39, 0}
 
\newcommand{\Vc}{\mathcal{V}_c}
\newcommand{\Vf}{\mathcal{V}_f}
\newcommand{\V}{\mathcal{V}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\K}{\mathcal{K}}
\newcommand{\X}{\mathbb{X}}
\newcommand{\Ss}{\mathbb{S}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\e}{\mathscr{E}}
\newcommand{\ee}{\mathcal{E}}
\newcommand{\Vol}{\Omega}
\newcommand{\task}{\mathcal{T}}

\newcommand{\Realdom}{\mathbf{R}}
\newcommand{\Intdom}{\mathbf{Z}}
\newcommand{\stoich}{\mathbb{N}}
\newcommand{\stoichd}{\mathbb{M}}
\newcommand{\Master}{\mathcal{M}}
\newcommand{\Diffuse}{\mathcal{D}}
\newcommand{\fatx}{\mathbf{x}}
\newcommand{\fatu}{\mathbf{u}}
\newcommand{\fatJ}{\mathbf{J}}
\newcommand{\fatE}{\mathbf{E}}
\newcommand{\fatP}{\mathbf{P}}
\newcommand{\fatD}{\mathbf{D}}
\newcommand{\fatmu}{\boldsymbol{\mu}}
\newcommand{\fatnu}{\boldsymbol{\nu}}
\newcommand{\fatlambda}{\boldsymbol{\Lambda}}
\newcommand{\Connect}{\mathbb{C}}

\begin{document}
\title{Efficiency and Parallelism in Discrete-Event Simulation}
\subtitle{Licentiate seminar}
\author{Pavol Bauer}
\institute[]{
  Division of Scientific Computing\\
  Department of Information Technology\\
  Uppsala University \\
  Sweden
}
\date{}
{\defbeamertemplate*{headline}{body}{
    \begin{beamercolorbox}{headline}%
      \vspace*{-2mm}\center
    \end{beamercolorbox}
  }
  {\usebackgroundtemplate{\includegraphics[width=\paperwidth]{sigill.png}}
    \frame{
	{
      \vspace{5mm}
      \hspace{0cm}
      \includegraphics[width=0.2\textwidth]{UU_logo_4f_84.pdf}
}
	{
      \vspace{-20mm}
      \hspace{8.5cm}
	\includegraphics[width=0.3\textwidth]{imgs/upmarc.eps}
	}
      \titlepage
    }
%upmarc logo
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Overview}
  \begin{itemize}
  \item Discrete-Event Simulation %(4m)
  \item Part I: Applications
  \begin{itemize}
		\item Sampling the Reaction-Diffusion Master Equation (Paper I \& III)%(10m)
		\item Simulation of Epidemics on Networks (Paper II)% (5m)
  \end{itemize}
  \item Part II: Methods 
  \begin{itemize}
	\item Parameter Sensitivity Estimation (Paper I)%(5m)
	\item Parallel Discrete-Event Simulation (Paper II \& III) %(15m)
  \end{itemize}
	\item Conclusion %(1m)
    \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Discrete-Event Simulation}
\framesubtitle{Definition of a discrete-event system}
Two characteristic properties:
\begin{enumerate}
\item The state space is a \textcolor{blue}{discrete set}.
\item The state transition mechanisms are \textcolor{blue}{event-driven}.
\end{enumerate}
Examples include finite-state automata, queues, Markov chains,...

\begin{center}
\includegraphics<1>[width=0.6\textwidth]{aiimgs/des0.eps}
\includegraphics<2>[width=0.6\textwidth]{aiimgs/des_sh1.eps}
\includegraphics<3>[width=0.6\textwidth]{aiimgs/des_sh2.eps}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Discrete-Event Simulation}
\framesubtitle{Typical simulator loop}
Consists of state, clock, event list, state update routine, ...
\begin{enumerate}
\item Set clock $t$ to initial time $t_0$ and state $\X$ to initial state $\X_0$
\item While the end time is not reached
 \begin{enumerate}
		\item Peek the event with the \textcolor{blue}{smallest event time} from the \textcolor{blue}{event list}.
		\item Apply it to the state using the \textcolor{blue}{state update routine}.
		\item Set the \textcolor{blue}{clock} to the time of the event.
		\item Update the \textcolor{blue}{event list}.
	\end{enumerate}
\end{enumerate}

\begin{center}
\includegraphics[width=0.6\textwidth]{aiimgs/dessim.eps}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Sampling the Reaction-Diffusion master equation}
  \framesubtitle{Motivating example: oscillations of the Min-system}

  Oscillations of proteins involved in the cell division of {\it
    E. coli}:
 
  \medskip

  \begin{tabular}{ll}
    $\mbox{MinD\_c\_atp} \xrightarrow{k_d} \mbox{MinD\_m}$ & 
    $\mbox{MinD\_c\_atp}+\mbox{MinD\_m} \xrightarrow{k_{dD}} \mbox{2MinD\_m} $ \\
    $\mbox{Min\_e+MinD\_m} \xrightarrow{k_{de}} \mbox{MinDE}$ & 
    $\mbox{MinDE} \xrightarrow{k_e} \mbox{MinD\_c\_adp} + \mbox{Min\_e} $ \\
    $\mbox{MinD\_c\_adp} \xrightarrow{k_p} \mbox{MinD\_c\_atp}$
  \end{tabular} 
  
\medskip
\begin{center}
  \begin{figure}[H]
%    \includegraphics[width=5cm]{imgs/minsweep_cycle.pdf} \qquad
%    \includegraphics[width=5cm]{imgs/MinD45n}
      \animategraphics[autostop,loop,height=4cm]{10}{anims/minanim/minanim_}{0}{99} 
  \end{figure}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Sampling the Reaction-Diffusion master equation}
  \framesubtitle{Spatial chemical kinetics}

- We sample  a \alert{continuous-time
   Markov chain} with event probability $P(\mbox{event in}~ [0,\Delta t]) =
  \mbox{\textcolor{blue}{rate}}\,\Delta t $.

\medskip

-State $\X \in \Intdom_{+}^{D}$, counting the number of
  molecules of each of $D$ species.

  -$R$ specified reactions defined as \emph{transitions} between these
  states,
  \begin{align*}
    &\X \xrightarrow{\textcolor{blue}{w_{r}(\X)}} \X-\stoich_{r}, 
	\qquad  \stoich \in \Intdom^{D \times R}  \mbox{ (\emph{stoichiometric matrix}) }
  \end{align*}
%\includegraphics[width=0.2\textwidth]{aiimgs/reaction.eps}
  under a transition intensity or \emph{propensity} $w_{r}$. 

\medskip
\pause

%  Let $p(\X,t) := P(\X(t) = \X |
  %\X(0))$. Then the \emph{chemical master equation} \alert{(CME)} is
  %given by
  %\begin{align*}
    %\frac{\partial p(\X,t)}{\partial t} &= 
    %\sum_{r = 1}^{R} w_{r}(\X+\stoich_{r})p(\X+\stoich_{r},t)-
    %\sum_{r = 1}^{R} w_{r}(\X)p(\X,t) 					\\
    %&=: \textcolor{blue}{\Master p},
  %\end{align*}
  %a gain-loss discrete PDE in $D$ dimensions for the probability.
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{frame}
%\frametitle{Sampling the Reaction-Diffusion master equation}
 %\framesubtitle{Adding spatial dependence}

  -Domain $\Vol$ is subdivided into $K$ smaller computational cells $\Vol_{j}$.
  Diffusion from one cell $\Vol_{k}$ to another cell $\Vol_{j}$ is

  \begin{align*}
    \X_{ik} &\xrightarrow{\textcolor{blue}{q_{kj} \X_{ik}}} \X_{ij},
  \end{align*}

  where $q_{kj}$ is non-zero only for connected cells. \\

- The state of the system is now an array $\X$ with $D \times
    K$ elements.

  %\pause
  %The \alert{diffusion master equation} can therefore be written
  %\begin{align*}
    %\nonumber
    %\frac{\partial p(\X, t)}{\partial t} =
    %\sum_{i=1}^{D} \sum_{k=1}^{K} \sum_{j=1}^{K}
    %&q_{kj}(\X_{ik}+\stoichd_{kj,k})
    %p(\X_{1 \cdot},\ldots, \X_{i \cdot}+\stoichd_{kj},\ldots,
    %\X_{D \cdot}, t) \\
    %-&q_{kj}\X_{ik}p(\X, t) =:
    %\textcolor{blue}{\Diffuse p(\X, t)}.
  %\end{align*}

%\pause

%\medskip
  %Finally, the \alert{reaction-diffusion master equation} (RDME) results from combining both operators,
  %\begin{align*}
    %\frac{\partial p(\X, t)}{\partial t} =
    %\textcolor{blue}{(\Master+\Diffuse) p(\X, t)}.
  %\end{align*}


\end{frame}
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Sampling the Reaction-Diffusion master equation}
  \framesubtitle{The Next Subvolume Method}

  Simulate a \textcolor{blue}{single stochastic trajectory} (``realization'') $\X(t)$;
\pause
  \begin{enumerate}

  \item[0.] Let $t = 0$ and set the state $\X$ to the initial number of
    molecules in each $j$ cell and compute the \textcolor{blue}{total rate $\lambda^{tot}_j$}$=\lambda^{r}_j+\lambda^{d}_j$.
	Generate the initial \textcolor{blue}{waiting times $\tau_j$} $\simeq Exp(1/(\lambda^{tot}_j))$ and store them in \textcolor{blue}{list $H$}.

\pause

  \item Remove the \textcolor{blue}{smallest time $\tau_j$} from $H$ and generate uniform RN $u_1$.

  \item If $u_1(\lambda^r_j+\lambda^d_j) \ge \lambda^r_j$, a \alert{diffusion} happened, otherwise a \alert{reaction}.

\pause

\item Find out \alert{which} reaction or diffusion happened by the requirement that
    \begin{align*}
      \sum_{s = 1}^{r-1} w_{s}(\X) &< \lambda^{r|d}_j u_{2} \le \sum_{s = 1}^{r} w_{s}(\X),
    \end{align*}
    where $u_{2}$ is again an uniform RN in and $\lambda^{r|d}_j := \sum_{r}
    w_{r}(\X_j)$ the total event intensity.

\pause

  \item Update the state by \textcolor{blue}{$\X_j := \X_j-\stoich_{r}$} (for reactions),
 compute a \textcolor{blue}{new waiting time $\tau_j$} and update the clock by \textcolor{blue}{$t
    := t+\tau_j$}. Reorder \textcolor{blue}{$H$}.

  \item Repeat from step 1 until some final time $T$ is reached.

  \end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  \item Compute the . Generate the \emph{time to the next reaction} $\tau :=
%    -W^{-1} \log u_{1}$ where $u_{1} \in (0,1)$ is a uniform random
%    number. Determine also the next reaction $r$ 

%%

\begin{frame}
\frametitle{Sampling the Reaction-Diffusion master equation}
  \framesubtitle{Path-wise representation}

The \emph{random time change representation} \alert{(RTC)} gives a path-wise representation of a single trajectory. It describes the state $\X_t$ 
as a sum of $R$  \textcolor{blue}{independent unit-rate Poisson processes $\Pi_{r}$},
\begin{align*}
  \X_{t} &= \X_{0}-\sum_{r = 1}^{R} \stoich_{r} \Pi_{r}
  \left(  \int_{0}^{t} w_{r}(\X_{t-}) \, dt \right),
\end{align*}
where $\X_{t-}$ is the state before any transitions at time $t$.

\medskip

Using the \emph{random counting measure}
$\mu_{r}(dt) = \mu_{r}(w_{r}(\X_{t-}) ; dt)$
for each $R$th event type  $\fatmu = [\mu_{1},\ldots,\mu_{R}]^{T}$ the RTC can be also written as
\begin{align*}
  \label{eq:SDE}
  d \X_{t} &= -\stoich \fatmu(dt),
\end{align*}
which is a \textcolor{red}{SDE with jumps}.


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Modeling of infection spread on networks}
  \framesubtitle{Motivating example: VTEC O15}

 Modeling and simulation of the verotoxinogenic E. coli (VTEC) {O15:H7} bacteria spread in the Swedish cattle population.
  \begin{multicols}{2}
	Stochastic transition between several age compartments
    \begin{itemize}
    \item $S \xrightarrow{s(t)~S} I$ (infection)
    \item $I \xrightarrow{r~I} S$ (recovery)
   \end{itemize}
  Deterministic events resulting from recorded data.
    \begin{itemize}
    \item $B$ - Enter
    \item $D$ - Exit
    \item $R$ - Aging
    \item $M$ - Transfer
%    \item Each event has an assigned time $t$ and source and/or destination nodes $ \X_1, \X_2$.
%    \item $\{E \in \{B,D,R,M\} , t, \X_1, \X_2\}$
    \end{itemize}
	\begin{figure}
%	\includegraphics[width=0.3\textwidth]{imgs/events.png}
	\animategraphics[autostop,loop,height=6cm]{5}{anims/transanim/transfers_}{0}{90} 
	\end{figure}
\end{multicols}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Modeling of infection spread on networks}
  \framesubtitle{Mathematical model}

Given $i=1 \ldots K$ nodes of an undirected graph $\mathcal{G}$, the state of each node $\X^{(i)}$ is described in terms of
\textcolor{red}{local dynamics} and \textcolor{blue}{network dynamics} in path-wise terms,

\medskip

\begin{equation*}
  d\X^{(i)}_{t} = \textcolor{red}{ \Ss \fatmu^{(i)}(dt) } -
  \textcolor{blue}{\sum_{j \in C(i)} \Connect\fatnu^{(i,j)}(dt)+
  \sum_{j; \, i \in C(j)} \Connect\fatnu^{(j,i)}(dt)}.
\end{equation*}

\medskip

\begin{itemize}
\item $\Ss, \Connect $ - transition matrices
\item $\boldsymbol{\mu^{(i)}}(dt)$ -  Poisson counting measure
\item $\boldsymbol{\nu^{(i,j)}, \nu^{(j,i)}}(dt)$ -  Dirac measures (but could be stochastic too)
\item $C(i)$ - connected components of node $i$
\end{itemize}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Modeling of infection spread on networks}
  \framesubtitle{Example: VTEC O15, numerical model}

To solve the model we \textcolor{blue}{discretize the time} and solve

  \begin{align*}
\X_{t+ \Delta t}' = \X_t + \int^{t+\Delta t}_t  \Ss \fatmu^{(i)}(dt) \\
\X_{t+ \Delta t} = \X_{t+ \Delta t}' + \int^{t+\Delta t}_t - \sum_{j \in C(i)} \Connect\fatnu^{(i,j)}(dt)+
  \sum_{j; \, i \in C(j)} \Connect\fatnu^{(j,i)}(dt)
\end{align*}

In practice:
\begin{itemize}
	\item Stochastic step: the \emph{Gillespie SSA}
	\item Data step: read \& incorporate deterministic event from {DB}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{frame}
%  \frametitle{Modeling of infection spread on networks}
%  \framesubtitle{Example: VTEC O15}

%\begin{figure}
%  \begin{center}
%    \includegraphics[width=0.9\textwidth]{imgs/herd-merge-370.png}
%  \end{center}
%\end{figure}

%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{\usebackgroundtemplate{\includegraphics[width=\paperwidth]{sigill.png}}
%  \frame{
%    \begin{center}
%      {\LARGE {\color{uppsalared} Part II: Methods}}
%    \end{center}
%  }
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Sensitivity estimation in stochastic systems (Paper I)}
\framesubtitle{Introduction}
\begin{columns}
  \begin{column}{0.5\textwidth}
\textcolor{blue}{Goal: } For a given perturbation $\delta$ of parameter $\theta$,
characterize the \alert{mean effect} on some function of interest, 
\begin{align*}
 	E[f(\X(t,\theta+\delta))-f(\X(t,\theta))].
  \end{align*}

\textcolor{blue}{Variance reduction problem: } 
the variance of functions
$f(\X(t,\theta+\delta))$ and $f(\X(t,\theta))$ \alert{can be large} compared to the
actual difference 
  \begin{align*}
f(\X(t,\theta+\delta))-f(\X(t,\theta)),
  \end{align*}
that we want to measure.
  \end{column}
  \begin{column}{0.5\textwidth}
    \centerline{\includegraphics[width=0.8\textwidth]{imgs/0dexample_small_arrows.eps}}
  \end{column}  
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Sensitivity estimation in stochastic systems (Paper I)}
\framesubtitle{Coupling of event paths}
  \begin{center}
  \includegraphics[width=0.25\linewidth]{../imgs/1percent}
 \includegraphics[width=0.25\linewidth]{../imgs/5percent}
 \includegraphics[width=0.25\linewidth]{../imgs/10percent}
  \end{center}
  A solution to the variance reduction problem is to obtain a stronger coupling of $f(\X(t,\theta+\delta))$ and $f(\X(t,\theta))$.
\begin{itemize}
	\item All events gets its own \textcolor{blue}{uniquely identifiable Poisson process} (given by a stream of random numbers)
	\item Discontinuity of events that produce a zero intensity (infinite waiting time) is handled
	\item Now we can compare the results of these models \textcolor{blue}{per trajectory}
\end{itemize}
$\Longrightarrow$ $E[\X_{t}(\theta+\delta)-\X_{t}(\theta)]^{2} = E[\X_{t}(\theta+\delta; \, \omega)-\X_{t}(\theta; \, \omega)]^{2} \alert{\sim O(\delta)}$.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Sensitivity estimation in stochastic systems (Paper I)}
\framesubtitle{The All Events Method: summary}
\begin{columns}
  \begin{column}{0.6\textwidth}
	  \begin{itemize}
	\item Advantages:
	\begin{itemize}
	\item The AEM \textcolor{blue}{reduces the variance} \alert{significantly better} than the Next Subvolume Method
	\item It can be used in combination with optimization algorithms to solve \textcolor{blue}{problems in inverse formulation}.
	\end{itemize}
	\item Costs:
	\begin{itemize}
	\item \textcolor{blue}{Memory consumption}: each event (and an associated RNG state) has to be stored separately in the event list 
	\item \textcolor{blue}{Sorting}: the sorting of the event list is thus more expensive (binary heap: $\mathcal{O}(\log{}n)$)
	\end{itemize}
\end{itemize}
	
  \end{column}
  \begin{column}{0.4\textwidth}
    \centerline{\includegraphics[width=\textwidth]{imgs/ode_upper_nopenalty.eps}}
    \centerline{\includegraphics[width=\textwidth]{imgs/ode_upper_penalty.eps}}
  \end{column}  
\end{columns}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallel Discrete Event Simulation}
 \framesubtitle{Introduction to Papers II \& III}

Techniques to parallelize discrete event simulation, for example:

\begin{itemize}
\item replicated trials \\
	- generation of multiple trajectories of the RDME
\item functional decomposition \\
	- parallelizing the state update routine 
\item time-parallel \\
	- splitting the time axis, e.g. via the \emph{Parareal} algorithm
\item space-parallel \\
	- classical domain decomposition
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallel Discrete Event Simulation}
 \framesubtitle{Space-parallel setup}
- State, Clock, Event list are private \\
- Events affecting another domain are \textcolor{blue}{communicated as messages}
	\begin{figure}
\centering
	\includegraphics[width=0.8\textwidth]{aiimgs/pdes.eps}
	\end{figure}
\pause
- \textcolor{blue}{Challenge:} process messages and local events in the correct order, i.e. maintain \alert{causal consistency}.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallel Discrete Event Simulation}
 \framesubtitle{Synchronization}

\begin{itemize}
\item conservative methods 
\begin{itemize}
\item causality errors \textcolor{blue}{are not allowed}
\item suitable for deterministic time steps
\item \textcolor{red}{Risc:} too much waiting time
\end{itemize}
\item optimistic methods 
\begin{itemize}
\item causality errors are \textcolor{blue}{firstly allowed}, but need to be later resolved using \textit{rollbacks}
\item suitable for stochastic time steps
\item \textcolor{red}{Risc:} too many roll-backs (``over-optimism'')
\end{itemize}
\end{itemize}
\medskip
	\begin{figure}
\centering
	\includegraphics[width=0.7\textwidth]{aiimgs/rollbacks.eps}
	\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallelism in the epidemic spread model (Paper II)}
 \framesubtitle{Introduction}
  
\begin{columns}
  \begin{column}{0.6\textwidth}

\begin{itemize}
\item Conservative PDES using task-based processing
\item Domain decomposition
\begin{itemize}
\item Split nodes into $k$ domains $h_1,\ldots,h_k$
\end{itemize}

\item Task decomposition
\begin{itemize}
	\item $\task_{S}(h,t)$ - Stochastic step and local data step (process events $B,D,R$) at domain $h$
	\item $\task_{M}(h_1,h_2,t)$ - Global data step  (process events $M$) between domains $h_1$ and $h_2$
\end{itemize}
\end{itemize}
\end{column}


 \begin{column}{0.4\textwidth}
\begin{figure}
\includegraphics[width=0.7\textwidth]{imgs/domain_decomp.jpg}
\end{figure}
\end{column}

\end{columns}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallelism in the epidemic spread model (Paper II)}
\framesubtitle{Handling of transfer events}
 
Since the network-dynamics are \textcolor{blue}{deterministic} and the time is \textcolor{blue}{discretized}, we can schedule 
$\task_M$  with dependencies that ensure the \alert{correct causal ordering}. Given two domains $\textcolor{uppsalagreen}{h_1}$ and $\textcolor{blue}{h_2} $,
\begin{align*}
\{ \task_{S}(\textcolor{uppsalagreen}{h_1},t_i) + \task_{S}(\textcolor{blue}{h_2},t_i) \} \prec \task_{M}(\textcolor{uppsalagreen}{h_1},\textcolor{blue}{h_i},t_i) \prec 
 \{ \task_{S}(\textcolor{uppsalagreen}{h_1},t_{i+1}) + \task_{S}(\textcolor{blue}{h_2},t_ {i+1}) \}
\end{align*}

Two scheduling policies:
\begin{itemize}
\item Coarse grained (one task contains all transfer events at $t_i$)
\item Fine grained (each transfer event at $t_i$ as an individual task )
\end{itemize}

\begin{figure}
	\includegraphics[width=0.7\textwidth]{imgs/sg_tasks.png}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallelism in the epidemic spread model (Paper II)}
 \framesubtitle{Results}

\begin{figure}
	\includegraphics[width=0.47\textwidth]{imgs/u10_coarse}
	\includegraphics[width=0.47\textwidth]{imgs/u10_fine}
\end{figure}

- In general, \textcolor{blue}{fine-grained} scheduling is favorable to the the \textcolor{blue}{coarse-grained} schedule.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallel sampling of RDME trajectories (Paper III)}
 \framesubtitle{Introduction}

\begin{itemize}
\item Time is continuous and time increments are \alert{exponentially distributed}
\item Hybrid optimistic / conservative synchronization algorithm based on the \textcolor{blue}{All Events Method}, this allows...

\begin{itemize}
\item Identifying the (pre-sampled) {event time when a \textcolor{blue}{diffusion event} communicates} between LPs
\item This time is communicated to the receiving LP as a Dynamic Local Time Window Estimate (\alert{DLTWE})
\item LPs can decide, based on the time, to halt the execution in order to \textcolor{blue}{minimize the risk of rollbacks}.
\end{itemize}
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallel sampling of RDME trajectories (Paper III)}
 \framesubtitle{Overview of DLTWE technique}

\begin{center}
\includegraphics<1>[width=0.9\textwidth]{aiimgs/dltwe0.eps}
\includegraphics<2>[width=0.9\textwidth]{aiimgs/dltwe1.eps}
\includegraphics<3>[width=0.9\textwidth]{aiimgs/dltwe2.eps}
\includegraphics<4>[width=0.9\textwidth]{aiimgs/dltwe3.eps}
\includegraphics<5>[width=0.9\textwidth]{aiimgs/dltwe4.eps}
\includegraphics<6>[width=0.9\textwidth]{aiimgs/dltwe4_2.eps}
\includegraphics<7>[width=0.9\textwidth]{aiimgs/dltwe5.eps}
\includegraphics<8>[width=0.9\textwidth]{aiimgs/dltwe5_2.eps}
\includegraphics<9>[width=0.9\textwidth]{aiimgs/dltwe6.eps}
\includegraphics<10>[width=0.9\textwidth]{aiimgs/dltwe7.eps}

\end{center}

\only<1-1>{Setup}
\only<2-2>{Setup}
\only<3-3>{Forward processing of \textcolor{darkgreen}{$e_3$} and \textcolor{darkgreen}{$e_6$} on LP~1 and 2.}
\only<4-4>{DLTWE set to \textcolor{red}{$e_5.t$} by LP~2.}
\only<5-5>{Comparison of \textcolor{darkgreen}{$e_2.t$} and \textcolor{red}{$e_5.t$} by LP~1.}
\only<6-6>{Forward processing of \textcolor{darkgreen}{$e_2$} on LP~1.}
\only<7-7>{Comparison of \textcolor{darkgreen}{$e_4.t$} and \textcolor{red}{$e_5.t$} by LP~1.}
\only<8-8>{Forward processing of \textcolor{darkgreen}{$e_8$} on LP~2.}
\only<9-9>{Forward processing of \textcolor{darkgreen}{$e_5$} on LP~2, includes sending message $m_1$ to LP~1.}
\only<10-10>{Forward processing of \textcolor{darkgreen}{$e_4$} on LP~1.}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Parallel sampling of RDME trajectories (Paper III)}
 \framesubtitle{Results}


\begin{center}
\includegraphics[width=0.9\textwidth]{imgs/dltwe_speedup.png}
\end{center}

\begin{columns}
  
 
\begin{column}{0.5\textwidth}
 Main performance indicators:
\begin{itemize}
 \item Inter-LP diffusion ratio (ratio of LP-private against inter-LP diffusion events)
 \item Model size (total number of cells)
\end{itemize}
  \end{column}
  \begin{column}{0.5\textwidth}
    \centerline{\includegraphics[width=0.9\textwidth]{imgs/fitting_lpd_size64.eps}}
  \end{column}  
\end{columns}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Conclusion}

\begin{itemize}
\item Paper I: we introduced an algorithm for efficient parameter sensitivity estimation in spatial stochastic system
\item Paper II: we defined a model for infectious disease spread on networks and detailed how to simulate the conservative PDES using dependency-aware task based scheduling 
\item Paper III: we introduced a hybrid optimistic/conservative PDES algorithm for efficient simulation of spatial stochastic models on multi-cores at a reduced probability of rollbacks
\end{itemize}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

{\usebackgroundtemplate{\includegraphics[width=\paperwidth]{sigill.png}}
  \frame{
    \begin{center}
      {\LARGE {\color{uppsalared} Acknowledgments} \\
	\medskip
	\large
	Stefan Engblom \\
	Stefan Widgren \\
	Jonatan Lind\'en \\
	Bengt Jonsson \\
	Martin Tillenius
}
    \end{center}
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
